# PIKA-POLYMER

[05/06/2018]

PIKA-POLYMER
APP with a pikachu SVG and Polymer

Im going to use Polymer 2.xx. Polymer 3.xx is not ready. I have a tool called "Polymer Modulizer" to transform proyect 2.xx to 3.xx

Why use Polymer 3.xx?

- Polymer becomes more compatible with the workflow and tools that a huge number of JavaScript developers are familiar with.
- Polymer elements and applications will run without any polyfills on recent versions of Chrome, Opera, and Safari. When Edge and Firefox ship custom elements and shadow DOM, Polymer will run polyfill-free on those browsers, too.
- You'll be able to work with regular JavaScript libraries more easily, whether you're importing a Polymer element into a library, or using a libraries inside an element.

What change?

- HTML Imports ➙ ES6 Modules (polyfills in components, ES6, )
- Bower ➙ npm

When will this happen?

When Polymer 3.xx is ok! Im going to transform 2.xx to 3.xx

COPYRIGHT Francisco Marquez Duro
